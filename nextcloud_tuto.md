<img title="Yasin Arıbuğa" src="./1200px-Nextcloud_Logo.png" alt="woman in white and black crew neck t-shirt playing chess" width="876" data-align="inline">

## Introduction :

Nextcloud propose sa plate-forme de collaboration de contenu open source et auto-hébergée, combinant ce qu'il décrit comme une interface utilisateur facile pour les solutions de cloud computing grand public avec les mesures de sécurité et de conformité dont les entreprises ont besoin. Nextcloud offre un accès universel aux données via des interfaces mobiles, de bureau et web, ainsi que des fonctions de communication et de collaboration sécurisées de nouvelle génération sur site, comme l'édition de documents en temps réel, le chat et les appels vidéo, ce qui les place sous le contrôle direct de l'informatique et les intègre à l'infrastructure existante.

**Site officiel :** [www.nextcloud.com](https://nextcloud.com)

**Documentation officiel :** <https://docs.nextcloud.com/server/25/admin_manual/>

## Prérequis :

Difficulté : moyen                                        Temps : 1h

* Une Raspberry Pi 4b ou supérieur
* Une carte SD de 16 Go ou plus
* Raspberry OS déjà installé
* Une une partition qui sera alloué au dossier data de nextcloud. Cela permet de faciliter l'ajout de HDD en cas de manque d'espace. (optionnelle)
* Deux ou trois tasses de café

Bien, maintenant que tout ça est prêt, nous pouvons commencer.

## Installation

La première chose que nous devons faire c'est installer un serveur Web. C'est lui qui va entrer en contact avec le navigateur du client. Il est là pour répondre aux requêtes qu'on lui donne. Il en éxiste une petite dizaine, mais les deux principaux sont :

* Apache
* Nginx

Nginx est connue pour sa capacité à gérer un grand nombre de connexion, ce qui l'a rendu très populaire, il est d'ailleurs utilisé par la plupart des serveurs qui reçoivent plusieurs milliers de requêtes par seconde. Apache est le serveur Web le plus utilisé et personnalisable notamment avec les fichiers .htaccess.

Dans ce tutoriel nous utiliserons Apache comme serveur Web pour des soucis de facilité.

Nous avons aussi besoin d'une base de donnée pour y stocker des informations comme les utilisateurs et les événements du calendrier... Les plus connues sont Mysql et Mariadb.

Dans ce tutoriel, nous utiliserons Mariadb mais vous pouvez prendre Mysql si vous le désirez, les commandes étant 100% compatibles entre les deux logiciels.

Nous aurons aussi besoin de php, là rien à dire dessus.

Nous allons commencer par installer la base de donnée et le serveur Web :

`apt-get install apache2 mariadb-server libApache2-mod-php7.4 openssl`

Puis php et quelques modules :

`apt-get install php7.4-gd php7.4-mysql php7.4-curl php7.4-mbstring php7.4-intl`

`apt-get install php7.4-gmp php7.4-bcmath php-imagick php7.4-xml php7.4-zip`

Nous allons vérifier qu'Apache s'est bien installé. Pour cela, ouvrez votre navigateur et tapez dans l'URL l'IP de votre serveur. Une page comme celle-ci devrait apparaître :

![Image](./apache-it-works.png)

Si ce n'est pas le cas, essayez de démarrer Apache avec `sudo systemctl start apache2`

Une fois installé, nous allons récupérer les fichiers Nextcloud.

Il suffit de se rendre sur cette page : [Install - Nextcloud](https://nextcloud.com/install/#instructions-server) et de faire un clic droit (sur Get ZIP file) puis copier l'adresse lien.



![](/home/adrien/Documents/Informatique/Tuto/nextcloud-installation-tuto/nextcloudInstallLink.png)



Dans un terminal du serveur, nous pouvons lancer la commande suivante :

```
wget https://download.nextcloud.com/server/releases/latest.zip
```

Le fichier va alors se télécharger dans le répertoire où vous êtes.

Le fichier actuellement compressé est inutilisable, nous devons donc le décompresser et le changer de répertoire.

`sudo unzip nextcloud-xxx.zip -d /var/www/`

La commande unzip avec l'option -d permet d'extraire le fichier dans le répertoire donné, ici : /var/www/ qui est le dossier réservé aux sites-Web. Vous pouvez, si vous le souhaitez changer de répertoire, mais je vous recommande ce répertoire.

Maintenant que notre fichier est prêt, nous devons nous assurer qu'Apache puisse y avoir accès avec la commande suivante :

`sudo chown -R www-data:www-data /var/www/nextcloud`

### Configurer Apache

Nous devons commencer par activer quelques modules nécessaire au bon fonctionement de Nextcloud.

```
a2enmod rewrite
a2enmod headers
a2enmod env
a2enmod dir
a2enmod mime
```

Petite explication de chaques modules :

* rewrite : Permet de modifier les URLs.
* headers : Permet de modifier les en-têtes des requêtes et des réponses HTTP.
* env : Permet de modifier les variables propre à Apache.
* dir : Permet la redirection d'url qui se termine avec un répertoire sans slash.
* mime : Associe des métadonnées à chaque type de fichier.

Une fois les modules activés, nous pouvons nous occuper de la configuration d'Apache.

Les fichiers de configurations d'Apache se trouvent généralement dans /etc/apache2/

Nous allons éditer le fichier suivant `/etc/apache2/sites-available/000-default.conf` mais avant de faire cela nous allons le renommer, car en effet `000-default.conf` ne veut pas dire grand chose.

```bash
sudo mv /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/nextcloud.conf
sudo nano /etc/apache2/sites-available/nextcloud.conf
```

```apacheconf
<VirtualHost *:80>
    # The ServerName directive sets the request scheme, hostname and port that
    # the server uses to identify itself. This is used when creating
    # redirection URLs. In the context of virtual hosts, the ServerName
    # specifies what hostname must appear in the request's Host: header to
    # match this virtual host. For the default virtual host (this file) this
    # value is not decisive as it is used as a last resort host regardless.
    # However, you must set it for any further virtual host explicitly.
    #ServerName www.example.com

    ServerAdmin Webmaster@localhost
    DocumentRoot /var/www/html

    # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
    # error, crit, alert, emerg.
    # It is also possible to configure the loglevel for particular
    # modules, e.g.
    #LogLevel info ssl:warn

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

    # For most configuration files from conf-available/, which are
    # enabled or disabled at a global level, it is possible to
    # include a line for only one particular virtual host. For example the
    # following line enables the CGI configuration for this host only
    # after it has been globally disabled with "a2disconf".
    #Include conf-available/serve-cgi-bin.conf
</VirtualHost>

# vim: syntax=Apache ts=4 sw=4 sts=4 sr noet
```

La balise `<VirtualHost *:80>` spécifie à Apache d'écouter sur le port 80 (http).

Je vous laisse décommenter la ligne suivante : `ServerName www.example.com` et mettre votre nom de domaine si vous en avez un ou votre IP dans le cas contraire.

`ServerAdmin` Correspond à votre email. `DocumentRoot` est le répertoire de nos fichiers Web, ici c'est /var/www/nextcloud/

Vous devez donc avoir un fichier qui ressemble à cela :

```apacheconf
<VirtualHost *:80>
    # The ServerName directive sets the request scheme, hostname and port that
    # the server uses to identify itself. This is used when creating
    # redirection URLs. In the context of virtual hosts, the ServerName
    # specifies what hostname must appear in the request's Host: header to
    # match this virtual host. For the default virtual host (this file) this
    # value is not decisive as it is used as a last resort host regardless.
    # However, you must set it for any further virtual host explicitly.

    ServerName www.domaine.com

    ServerAdmin admin@domaine.com
    DocumentRoot /var/www/nextcloud

    # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
    # error, crit, alert, emerg.
    # It is also possible to configure the loglevel for particular
    # modules, e.g.
    #LogLevel info ssl:warn

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

    # For most configuration files from conf-available/, which are
    # enabled or disabled at a global level, it is possible to
    # include a line for only one particular virtual host. For example the
    # following line enables the CGI configuration for this host only
    # after it has been globally disabled with "a2disconf".
    #Include conf-available/serve-cgi-bin.conf
</VirtualHost>

# vim: syntax=Apache ts=4 sw=4 sts=4 sr noet
```

Nous avons aussi besoin de rajouter quelques petites règles supplémentaires pour la sécurité de notre serveur qui s'appliquera sur le port 80(http) et 443(https). Pour cela, nous allons rajouter quelques lignes au début de notre fichier de configuration.

```apacheconf
Alias /nextcloud "/var/www/nextcloud/"

<Directory /var/www/nextcloud/>
  Require all granted
  AllowOverride All
  Options FollowSymLinks MultiViews

  <IfModule mod_dav.c>
    Dav off
  </IfModule>

</Directory>
```

**Penser à modifier les valeurs en fonction de votre configuration.**

```apacheconf
Require all granted
```

Permet d'autoriser toutes les IP à s'y connecter.

```apacheconf
 AllowOverride All
```

Indique qu'il y a des fichiers de configuration externe ( fichier .htaccess) qui sont présents et qu'Apache doit les lire.

Votre fichier doit ressembler à cela (j'ai supprimé les commentaires pour plus de visibilité.):

```apacheconf
Alias /nextcloud "/var/www/nextcloud/"

<Directory /var/www/nextcloud/>
  Require all granted
  AllowOverride All
  Options FollowSymLinks MultiViews

  <IfModule mod_dav.c>
    Dav off
  </IfModule>

</Directory>

<VirtualHost *:80>

    ServerName www.domaine.com

    ServerAdmin admin@domaine.com
    DocumentRoot /var/www/nextcloud

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>

# vim: syntax=Apache ts=4 sw=4 sts=4 sr noet
```

Puis nous activons notre configuration et redémarrons le service Apache pour appliquer les changements :

```bash
a2ensite nextcloud.conf #Permet d'activer notre fichier de configuration
sudo systemctl restart apache2 #Redémarre Apache
```

Maintenant que la configuration d'Apache est fini (pour le moment) nous pouvons nous attaquer à mariadb :)

### Configurer Mariadb

Nous allons commencer par terminer la configuration de mariadb avec la commande ci-dessous :

```sql
mysql_secure_installation
```

Mysql va alors nous poser tout un tas de question auquel il va falloir répondre.

**Set root password? [Y/n] :**

```
Y
```

**Remove anonymous users? [Y/n] :**

```
Y
```

**Disallow root login remotely? [Y/n] :**

```
Y
```

**Remove test database and access to it? [Y/n] :**

```
Y
```

**Reload privilege tables now? [Y/n] :**

```
Y
```

Voici le résumé des réponses :

```
Set root password? [Y/n] Y
New password: 
Re-enter new password: 
 ... Success!

Remove anonymous users? [Y/n] Y
 ... Success!

Disallow root login remotely? [Y/n] Y
 ... Success!

Remove test database and access to it? [Y/n] Y
 ... Success!

Reload privilege tables now? [Y/n] Y
 ... Success!

Cleaning up...

All done!  If you've completed all of the above steps, your MariaDB
installation should now be secure.

Thanks for using MariaDB!
```

Nous avons aussi besoin de créer un autre utilisateur spécifique à Nextcloud. Rentrons pour cela dans mysql en rentrant simplement : `mysql` Puis créer l'utilisateur avec :

```sql
CREATE USER 'nextcloud_user'@'localhost' IDENTIFIED BY 'password';
```

Bien sûr, n'oubliez pas de changer de mot de passe :)

Puis la base de donnée :

```sql
CREATE DATABASE IF NOT EXISTS nextcloud_db CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
```

Puis nous donnons les droits à notre utilisateur Nextcloud :

```sql
GRANT ALL PRIVILEGES ON nextcloud_db.* TO 'nextcloud_user'@'localhost';
```

On applique les changements :

```sql
FLUSH PRIVILEGES;
```

### Configurer Nextcloud

Bien, maintenant il ne reste plus qu'à nous connecter sur notre site Web :)

Rentrer l'adresse IP de votre serveur dans votre naviguateur et... magie cette page devrait apparaître :

Si c'est le cas bravo sinon c'est que vous avez loupé un truc. :)

![alt text](/home/adrien/Documents/Informatique/Tuto/nextcloud-installation-tuto/Nextcloud-config.png)

Là, rien de plus simple. Entrer votre nouvel identifiant et mot de passe (Ce compte est un compte administrateur. Ne l'utilisez pas comme un compte personnel !!)

Pour le dossier data deux possibilitées :

Soit vous avez crée une autre partition pour le dossier data, dans ce cas changez le répertoire du dossier (N'oubliez pas de donner les droits à Apache avec sudo chown -R www-data:www-data /votre/partition !!) ou sinon vous pouvez laisser par défaut.

Pour le reste, mettez les infos qu'il nous demande :

```
Utilisateur de la base de donnée : nextcloud_user
Mot de passe de l'utilisateur : password
Nom de la base de donnée : nextcloud_db
```

Ce n'est là qu'un èxemple, mettez les informations que vous avez défini plus haut !! Une fois complété, vous pouvez valider. Remarquez que Nextcloud vous demande si vous voulez installer des plugins comme le calendrier, only-office et autre, à vous de choisir.

Roulement de tambour................

![alt text](/home/adrien/Documents/Informatique/Tuto/nextcloud-installation-tuto/Screenshot%20from%202022-10-20%2016-39-20.png)

OUI !!!!!

Cependant ce n'est que le commencement :)

## Configurer le cache

Pour que Nextcloud soit plus performant, il est recommandé de configurer une mémoire cache. Elle permet de faciliter l'accès aux fichiers dans le temps.

Pour cela il suffit d'installer le module php suivant : `php-apcu`

Puis de rajouter cette ligne `'memcache.local' => '\OC\Memcache\APCu',` dans le config.php présent dans la racine de votre installation dans le dossier config, soit `/var/www/nextcloud/config/config.php` dans notre cas.

Le fichier config.php est un fichier de configuration, c'est là où tous les paramètres de Nextcloud comme la base de donnée utilisé, le serveur smpt pour l'envoi d'email et encore plein d'autres paramètres y sont stockés.

Puis `sudo systemctl restart apache2` pour appliquer.

## Basculer en HTTPS

Comme vous avez pu le remarquer, il y a en haut à gauche dans votre naviguateur un petit message vous indiquant que votre connection n'est pas sécurisée. En effet pour le moment toute les informations circulent en clair, ce qui veut dire qu' en théorie, tout le monde peut voir ce qui ce passe. Ce qui n'est pas franchement génial.

La solution, passé en https afin de rendre illisible l'information afin que personne ne puisse la lire.

#### Générer une paire de clés publiques/privées

Commençons par générer une paire de clés RSA de 2 048 bits. Une clé plus petite, telle que 1 024 bits, n'est pas suffisamment résistante aux attaques par force brute. Une clé plus grande, telle que 4 096 bits, est exagérée. Au fil du temps, la taille des clés augmente à mesure que le traitement informatique devient moins cher. 2 048 bits est actuellement la taille idéale.

Source : [Enabling HTTPS on Your Servers](https://developers.google.com/web/fundamentals/security/encrypt-in-transit/enable-httpshttps://developers.google.com/web/fundamentals/security/encrypt-in-transit/enable-https)

La commande pour générer la paire de clés RSA est la suivante :

```bash
openssl genrsa -out www.example.com.key 2048
```

#### Générer une demande de signature de certificat

Dans cette étape, nous intégrons notre clé publique et des informations sur notre organisation et notre site Web dans une demande de signature de certificat ou CSR. La commande openssl nous demande de manière interactive les métadonnées requises.

```bash
openssl req -new -sha256 -key www.example.com.key -out www.example.com.csr
```

Puis nous vérifions notre fichier avec :

```bash
openssl req -text -in www.example.com.csr -noout
```

La sorti doit ressembler à cela :

```
Certificate Request:
    Data:
        Version: 1 (0x0)
        Subject: C = FR, ST = FR, L = FR, O = FR, OU = FR, CN = FR, emailAddress = FR
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    00:b0:81:14:f7:f4:8a:2a:21:7d:d0:01:64:d4:11:
                    3e:08:f8:82:a0:53:7c:c7:76:f7:e5:aa:31:7c:d1:
                    54:00:63:90:d3:9a:da:c9:35:e2:48:c5:4f:d6:df:
                    6a:f1:61:8a:c5:6f:fb:c6:34:a7:3e:81:6d:3a:63:
                    8f:02:e8:09:24:3e:a0:f5:58:53:97:d6:ff:41:7b:
                    07:5b:33:69:ce:fc:9c:d8:ef:21:6f:86:06:11:99:
                    25:d6:35:1e:2a:f5:86:a8:a7:a5:8f:80:33:50:f4:
                    37:4e:8c:c5:06:4a:8c:78:fc:8e:2e:ad:1e:b7:a4:
                    70:e0:f6:95:d4:90:4a:85:f9:1a:99:71:0b:a8:39:
                    e0:f9:88:93:fe:71:f8:cc:b3:7c:bb:33:11:c8:76:
                    ca:15:ed:92:68:41:98:1b:2e:49:19:08:84:a6:67:
                    5a:df:23:6c:23:14:7f:2e:7b:d3:f5:99:e9:da:36:
                    c2:99:af:09:06:4b:44:71:76:30:66:35:a0:c6:1d:
                    a5:34:5f:d2:37:4d:48:e8:b5:c7:b1:e2:62:c6:80:
                    74:4e:e5:f1:13:ec:c0:d5:e2:79:10:b4:2f:68:34:
                    42:4c:fb:1f:f5:6f:30:4b:9b:9d:f4:a9:9f:4c:dc:
                    98:28:c4:f0:b4:92:05:65:ef:5d:57:33:e3:d4:e1:
                    ac:2d
 ...
```

Et enfin nous générons le certificat auto-signé valable pendant 365 jours :

```bash
sudo openssl x509 -req -days 365 -in www.example.com.csr -signkey www.example.com.key -out www.example.com.crt
```

Maintenant, que nous avons généré notre certificat, nous pouvons le mettre en lieu sûr.

```bash
sudo cp www.example.com.crt /etc/ssl/www_example/www.example.com.crt
sudo cp www.example.com.key /etc/ssl/www_example/www.example.com.key
```

Vous pouvez noter les chemins, car nous devrons les renseigner un peu plus tard dans le fichier de configuration d'Apache.

#### Activer HTTPS sur notre serveur

Nous allons commencer par activer le module ssl dans Apache :

```bash
a2enmod ssl
```

Puis modifier le fichier /etc/apache2/sites-available/nextcloud.conf et rajouter la ligne suivante :

```apacheconf
Redirect permanent / https://votre-ip-ou-nom-de-domaine/
```

Redirect permanent va permettre de rediriger le flux http vers l'https, ainsi si un utilisateur rentre l'adresse du serveur sans le "https"celui-ci sera redirigé dessus.

La ligne ci-dessous nous intéressant plus, nous pouvons la commenter.

```apacheconf
DocumentRoot /var/www/nextcloud
```

Votre fichier doit maintenant ressembler à ceci :

```apacheconf
Alias /nextcloud "/var/www/nextcloud/"

<Directory /var/www/nextcloud/>
  Require all granted
  AllowOverride All
  Options FollowSymLinks MultiViews

  <IfModule mod_dav.c>
    Dav off
  </IfModule>

</Directory>

<VirtualHost *:80>
    ServerName www.domaine.com    

    Redirect permanent / https://votre-ip-ou-nom-de-domaine/

    ServerAdmin admin@domaine.com

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>

# vim: syntax=Apache ts=4 sw=4 sts=4 sr noet
```

Bien, nous avons fait une redirection, mais elle pointe nul part, c'est pas très pratique :).

Pour une configuration https Apache nous donne un exemple dans `/etc/apache2/site-available/default-ssl.conf`, nous n'allons pas l'utiliser directement, mais nous allons nous en inspirer.

Vous pouvez l'ouvrir avec :

```bash
nano /etc/apache2/site-available/default-ssl.conf
```

Vous devriez avoir ceci :

```apacheconf
<IfModule mod_ssl.c>
    <VirtualHost _default_:443>
        ServerAdmin Webmaster@localhost

        DocumentRoot /var/www/html

        # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
        # error, crit, alert, emerg.
        # It is also possible to configure the loglevel for particular
        # modules, e.g.
        #LogLevel info ssl:warn

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

        # For most configuration files from conf-available/, which are
        # enabled or disabled at a global level, it is possible to
        # include a line for only one particular virtual host. For example the
        # following line enables the CGI configuration for this host only
        # after it has been globally disabled with "a2disconf".
        #Include conf-available/serve-cgi-bin.conf

        #   SSL Engine Switch:
        #   Enable/Disable SSL for this virtual host.
        SSLEngine on

        #   A self-signed (snakeoil) certificate can be created by installing
        #   the ssl-cert package. See
        #   /usr/share/doc/Apache2/README.Debian.gz for more info.
        #   If both key and certificate are stored in the same file, only the
        #   SSLCertificateFile directive is needed.
        SSLCertificateFile    /etc/ssl/certs/ssl-cert-snakeoil.pem
        SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key

        #   Server Certificate Chain:
        #   Point SSLCertificateChainFile at a file containing the
        #   concatenation of PEM encoded CA certificates which form the
        #   certificate chain for the server certificate. Alternatively
        #   the referenced file can be the same as SSLCertificateFile
        #   when the CA certificates are directly appended to the server
        #   certificate for convinience.
        #SSLCertificateChainFile /etc/Apache2/ssl.crt/server-ca.crt

        #   Certificate Authority (CA):
        #   Set the CA certificate verification path where to find CA
        #   certificates for client authentication or alternatively one
        #   huge file containing all of them (file must be PEM encoded)
        #   Note: Inside SSLCACertificatePath you need hash symlinks
        #         to point to the certificate files. Use the provided
        #         Makefile to update the hash symlinks after changes.
        #SSLCACertificatePath /etc/ssl/certs/
        #SSLCACertificateFile /etc/Apache2/ssl.crt/ca-bundle.crt

        #   Certificate Revocation Lists (CRL):
        #   Set the CA revocation path where to find CA CRLs for client
        #   authentication or alternatively one huge file containing all
        #   of them (file must be PEM encoded)
        #   Note: Inside SSLCARevocationPath you need hash symlinks
        #         to point to the certificate files. Use the provided
        #         Makefile to update the hash symlinks after changes.
        #SSLCARevocationPath /etc/Apache2/ssl.crl/
        #SSLCARevocationFile /etc/Apache2/ssl.crl/ca-bundle.crl

        #   Client Authentication (Type):
        #   Client certificate verification type and depth.  Types are
        #   none, optional, require and optional_no_ca.  Depth is a
        #   number which specifies how deeply to verify the certificate
        #   issuer chain before deciding the certificate is not valid.
        #SSLVerifyClient require
        #SSLVerifyDepth  10

        #   SSL Engine Options:
        #   Set various options for the SSL engine.
        #   o FakeBasicAuth:
        #     Translate the client X.509 into a Basic Authorisation.  This means that
        #     the standard Auth/DBMAuth methods can be used for access control.  The
        #     user name is the `one line' version of the client's X.509 certificate.
        #     Note that no password is obtained from the user. Every entry in the user
        #     file needs this password: `xxj31ZMTZzkVA'.
        #   o ExportCertData:
        #     This exports two additional environment variables: SSL_CLIENT_CERT and
        #     SSL_SERVER_CERT. These contain the PEM-encoded certificates of the
        #     server (always existing) and the client (only existing when client
        #     authentication is used). This can be used to import the certificates
        #     into CGI scripts.
        #   o StdEnvVars:
        #     This exports the standard SSL/TLS related `SSL_*' environment variables.
        #     Per default this exportation is switched off for performance reasons,
        #     because the extraction step is an expensive operation and is usually
        #     useless for serving static content. So one usually enables the
        #     exportation for CGI and SSI requests only.
        #   o OptRenegotiate:
        #     This enables optimized SSL connection renegotiation handling when SSL
        #     directives are used in per-directory context.
        #SSLOptions +FakeBasicAuth +ExportCertData +StrictRequire
        <FilesMatch "\.(cgi|shtml|phtml|php)$">
                SSLOptions +StdEnvVars
        </FilesMatch>
        <Directory /usr/lib/cgi-bin>
                SSLOptions +StdEnvVars
        </Directory>

        #   SSL Protocol Adjustments:
        #   The safe and default but still SSL/TLS standard compliant shutdown
        #   approach is that mod_ssl sends the close notify alert but doesn't wait for
        #   the close notify alert from client. When you need a different shutdown
        #   approach you can use one of the following variables:
        #   o ssl-unclean-shutdown:
        #     This forces an unclean shutdown when the connection is closed, i.e. no
        #     SSL close notify alert is send or allowed to received.  This violates
        #     the SSL/TLS standard but is needed for some brain-dead browsers. Use
        #     this when you receive I/O errors because of the standard approach where
        #     mod_ssl sends the close notify alert.
        #   o ssl-accurate-shutdown:
        #     This forces an accurate shutdown when the connection is closed, i.e. a
        #     SSL close notify alert is send and mod_ssl waits for the close notify
        #     alert of the client. This is 100% SSL/TLS standard compliant, but in
        #     practice often causes hanging connections with brain-dead browsers. Use
        #     this only for browsers where you know that their SSL implementation
        #     works correctly.
        #   Notice: Most problems of broken clients are also related to the HTTP
        #   keep-alive facility, so you usually additionally want to disable
        #   keep-alive for those clients, too. Use variable "nokeepalive" for this.
        #   Similarly, one has to force some clients to use HTTP/1.0 to workaround
        #   their broken HTTP/1.1 implementation. Use variables "downgrade-1.0" and
        #   "force-response-1.0" for this.
        # BrowserMatch "MSIE [2-6]" \
        #        nokeepalive ssl-unclean-shutdown \
        #        downgrade-1.0 force-response-1.0

    </VirtualHost>
</IfModule>

# vim: syntax=Apache ts=4 sw=4 sts=4 sr noe
```

```apacheconf
<IfModule mod_ssl.c>
    <VirtualHost _default_:443>
        ...
        SSLEngine on
        ...
    </VirtualHost>
</IfModule>
```

Nous retrouvons un VirtualHost mais qui écoute sur le port 443 (https) et la balise `<IfModule mod_ssl.c>` qui permet d'initialiser le module ssl. Nous retrouvons aussi la directive `SSLEngine on` pour activer le support de ssl.

Je vous laisse ajouter tout cela à votre `nextcloud.conf` en n'oubliant pas de rajouter votre DocumentRoot, ServerName et ServerAdmin.

Bien que votre serveur fonctionne en https il est loin d'être entièrement sécurisé, en effet il reste compatible avec de vieux algorithmes de chiffrement qui ne sont plus utilisé aujourd'hui, leur niveau de sécurité étant trop faible. Pour remédier à cela je vous invite à visiter le site [ssl-config.mozilla](https://ssl-config.mozilla.org/), pour vous aider à renforcer la sécurité de votre serveur.

Dans le cas d'une configuration moderne quelques ligne nous sont pour le moment inconnues.

```apacheconf
Protocols h2 http/1.1
```

Cette ligne permet simplement de spécifier à Apache d'utiliser HTTP/2 et s'il n'est pas disponnible HTTP/1.1. Par défaut HTTP/2 n'est pas activé nous allons donc devoir l'activer un peu plus tard.

```apacheconf
 Header always set Strict-Transport-Security "max-age=63072000"
```

Utilisez Strict Transport Security pour indiquer aux clients qu'ils doivent toujours se connecter à votre serveur via HTTPS, même lorsqu'ils suivent une référence http://. Cela permet de déjouer des attaques telles que le SSL Stripping et d'éviter le coût de l'aller-retour de la redirection 301.

```apacheconf
# modern configuration
SSLProtocol             all -SSLv3 -TLSv1 -TLSv1.1 -TLSv1.2
SSLHonorCipherOrder     off
SSLSessionTickets       off
```

Nous autorisons uniquement les protocoles les plus forts en excluant SSLv3 TLSv1 TLSv1.1 TLSv1.2.

```apacheconf
SSLCertificateFile    /etc/ssl/certs/ssl-cert-snakeoil.pem
SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key
```

Je vous laisse ici modifier le chemin de vos clés que nous avons généré précédemment. Si je reprends l'exemple au-dessus de cela, donne :

```apacheconf
SSLCertificateFile    /etc/ssl/www_example/www.example.com.crt
SSLCertificateKeyFile /etc/ssl/www_example/www.example.com.key
```

Votre fichier `nextcoud.conf` doit maintenant ressembler à cela :

```apacheconf
Alias /nextcloud "/var/www/nextcloud/"

<Directory /var/www/nextcloud/>
  Require all granted
  AllowOverride All
  Options FollowSymLinks MultiViews

  <IfModule mod_dav.c>
    Dav off
  </IfModule>

</Directory>

<VirtualHost *:80>

    ServerName www.domaine.com    

    Redirect permanent / https://votre-ip-ou-nom-de-domaine/

    ServerAdmin admin@domaine.com
    #DocumentRoot /var/www/nextcloud

    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>

<IfModule mod_ssl.c>
    <VirtualHost _default_:443>

        SSLEngine on

        Protocols h2 http/1.1

        Header always set Strict-Transport-Security "max-age=63072000"

        ServerName www.domaine.com
        ServerAdmin admin@domaine.com

        DocumentRoot "/var/www/nextcloud/"

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

        SSLCertificateFile    /etc/ssl/www_example/www.example.crt
        SSLCertificateKeyFile /etc/ssl/www_example/www.example.key

    </VirtualHost>
</IfModule>

# modern configuration
SSLProtocol             all -SSLv3 -TLSv1 -TLSv1.1 -TLSv1.2
SSLHonorCipherOrder     off
SSLSessionTickets       off

# vim: syntax=Apache ts=4 sw=4 sts=4 sr noet
```

### 

Rechargez la configuration avec :

```bash
sudo systemctl restart apache2
```

<img title="" src="./error_ssl.png" alt="k" width="769">

Oups !!!

Non, c'est normal ! Et oui, nous sommes avec un certificat auto-signé c'est donc à nous de le certifié. Nous avons juste besoin d'aller dans "avancer" puis accepter le certificat. Et voilà nous somme sur Nextcloud en https.

## Signer le certificat (optionnelle)

Bien, maintenant que votre serveur fonctionne à merveille, vous voudriez peut-être le rendre plus facile d'accès. Par exemple, quand vous vous connectez, autoriser à chaque fois le 
certificat peut-être redondant, dans ce cas la certification du 
certificat s'impose. Ce n'est pas le but de ce tuto de vous montrer comment faire, mais il y a des solutions comme Let's encrypt qui le propose gratuitement.  

Je vous renvoie vers le tuto d'Alban qui l'explique très bien : [Activation du module SSL/TLS](https://trevilly.com/installation-de-nextcloud-21-avec-apache-php-et-mariadb/#SSL)  

**!! Vous devez avoir un nom de domaine au préalable !!**

## Activer HTTP2 (optionnelle)

Les principaux objectifs de HTTP/2 sont de réduire la latence en permettant le multiplexage complet des demandes et des réponses, de minimiser la surcharge du protocole par une compression efficace des champs d'en-tête HTTP et d'ajouter la prise en charge de la hiérarchisation des demandes et du push serveur. Il existe un grand nombre d'autres améliorations du protocole, telles que de nouveaux mécanismes de contrôle de flux, de traitement des erreurs et de mise à niveau.

Source : [Introduction to HTTP/2]([Introduction to HTTP/2 &nbsp;|&nbsp; Web Fundamentals &nbsp;|&nbsp; Google Developers](https://developers.google.com/web/fundamentals/performance/http2?hl=en))

Dans un premier temps, nous devons désactiver quelques modules qui ne sont pas compatibles avec HTTP2.

`sudo a2dismod php7.3`

Par défaut, Apache utilise le module MPM (module multi-processus) prefork. Ce MPM (module multi-processus) n'est pas compatible avec HTTP/2, nous devons donc le remplacer par le module mpm_event, plus moderne.
`sudo a2dismod mpm_prefork`
`sudo a2enmod mpm_event`

`sudo a2enconf php7.3-fpm`
`sudo a2enmod proxy_fcgi`

Et enfin :
`sudo a2enmod http2`

`sudo systemctl restart apache2`

Si vous obtenez une erreur essayé de redémarrer votre Raspberry Pi.

##### Expliquation des modules : prefork et event

###### Prefork

Ce module multi-processus (MPM) implémente un serveur web avec démarrage anticipé de processus. Chaque processus du serveur peut répondre aux requêtes entrantes, et un processus parent contrôle la taille du jeu de processus enfants. C'est également le MPM le plus approprié si l'on veut isoler les requêtes les unes des autres, de façon à ce qu'un problème concernant une requête n'affecte pas les autres.

Source : [Apache]([prefork - Serveur HTTP Apache Version 2.4](https://httpd.apache.org/docs/current/fr/mod/prefork.html))

###### Event

Le module multi-processus (MPM) event est conçu  pour permettre le traitement d'un nombre accru de requêtes simultanées en déléguant certaines tâches aux threads d'écoute, libérant par là-même les threads de travail et leur permettant de traiter les nouvelles requêtes.

Source : [Apache]([event - Serveur HTTP Apache Version 2.4](https://httpd.apache.org/docs/2.4/fr/mod/event.html))

## Configurer Fail2ban (optionnelle)

Si vous décidez de rendre accessible votre serveur depuis l'extérieur il est fortement probable qu'il devienne une cible pour des personnes malveillantes. Pour se prémunir contre d'éventuelles attaques de ce type nous allons utiliser le logiciel Fail2ban qui utilise les tables iptables pour bannir les éventuelles IPs qui chercheraient à se connecter de manière répétée et avec sans succès. Voici un lien pour plus d'informations : https://doc.ubuntu-fr.org/fail2ban.

Dans un premier temps, nous devons installer le logiciel :

```bash
sudo apt install fail2ban
```

Nous allons maintenant créer le filtre de configuration propre à nextcloud :

```bash
nano /etc/fail2ban/filter.d/nextcloud.conf
```

Et y mettre ceci :

```bash
[Definition]
_groupsre = (?:(?:,?\s*"\w+":(?:"[^"]+"|\w+))*)
failregex = ^\{%(_groupsre)s,?\s*"remoteAddr":"<HOST>"%(_groupsre)s,?\s*"message":"Login failed:
            ^\{%(_groupsre)s,?\s*"remoteAddr":"<HOST>"%(_groupsre)s,?\s*"message":"Trusted domain error.
datepattern = ,?\s*"time"\s*:\s*"%%Y-%%m-%%d[T ]%%H:%%M:%%S(%%z)?"
```

Cela permet de définir les règles de bannissement pour identifier les authentifications ratées depuis les logs de nextcloud. Cela s'applique pour l'interface Web, WebDav et les sous domaine non autorisés.

Nous devons maitenant créer un autre fichier pour définir les règles du bannisement.

```bash
nano /etc/fail2ban/jail.d/nextcloud.conf
```

Pour y mettre :

```bash
[nextcloud]
backend = auto
enabled = true
port = 80,443
protocol = tcp
filter = nextcloud
maxretry = 3
bantime = 86400
findtime = 43200
logpath = /var/www/nextcloud/data/nextcloud.log
```

Ici, rien à modifier, sauf si vous utilisez des ports différents. Le ```bantime``` est le temps que le serveur sera indisponible pour l'attaquant, ici 86400s soit 24h. Le `findtime` correspond au nombre d'essais maximum (maxretry) sur la pèriode défini.

Nous pouvons appliquer les changements avec :

```bash
sudo systemctl restart fail2ban
```

Bien, nous pouvons vérifier que tout fonctionne correctement avec :

```bash
fail2ban-client status nextcloud
```

Nous en avons finit avec fail2ban, il est temps de nous rendre sur notre serveur depuis le navigateur.

<hr>

## Problèmes connus :

```
The database is missing some indexes. Due to the fact that adding 
indexes on big tables could take some time they were not added 
automatically. By running "occ db:add-missing-indices" those missing 
indexes could be added manually while the instance keeps running. 
Once the indexes are added queries to those tables are usually much faster.
    - Missing index "calendarobject_calid_index" in table "oc_calendarobjects_props".
    - Missing index "schedulobj_principuri_index" in table "oc_schedulingobjects".
```

Exécutez la commande suivante :

```bash
sudo -u www-data php /votre/dossier/nextcloud/occ db:add-missing-indices
```

---

```
Some columns in the database are missing a conversion to big int.
Due to the fact that changing column types on big tables could take some
time they were not changed automatically. By running 'occ db:convert-filecache-bigint'
those pending changes could be applied manually. This operation needs to
be made while the instance is offline. For further details read the 
documentation page about this.
    mounts.storage_id
    mounts.root_id
    mounts.mount_id
```

Exécutez la commande suivante :

```bash
sudo -u www-data php /votre/dossier/nextcloud/occ db:convert-filecache-bigint
```

---

```
The PHP memory limit is below the recommended value of 512MB.
```

Editer le fichier suivant /etc/php/7.3/Apache2/php.ini, chercher la ligne `memory_limit` (CTRL + w avec nano) puis remplacer la valeur par 512M.

Redémarrer Apache avec :

```bash
sudo systemctl restart Apache2
```

---

```
Module php-imagick in this instance has no SVG support. For better compatibility it is recommended to install it.
```

Exécutez la commande suivante :

```bash
sudo apt install libmagickcore-6.q16-6-extra
```

---

```
Your installation has no default phone region set. 
This is required to validate phone numbers in the profile settings
 without a country code. To allow numbers without a country code, 
please add "default_phone_region" with the respective ISO 3166-1 code ↗ 
of the region to your config file.
```

Editer le fichier suivant /var/www/nextcloud/config/config.php, et ajouter cette ligne :

`'default_phone_region' => 'FR'`

---

## Aller plus loin :

#### Nextcloud depuis l'extérieur

Avoir accès à votre serveur Nextcloud en local c'est bien, mais depuis l'extérieur c'est mieux. :)

Ce n'est pas ici l'objet de ce tuto de vous montrer comment faire, car les manipulation varie en fonction de votre opérateur et des box. Une chose est sûre, c'est qu'il faut ouvrir les ports 80/tcp et 443/tcp dans les tables NAT/PAT, avoir une IP statique afin d'éviter de se connecter chez le voisin à chaque fois que votre box redémarre (c'est un service qui peut être payant selon votre opérateur) et n'oubliez pas d'autoriser l'IP dans le fichier de config.php de votre serveur Nextcloud.

#### Toujours plus d'espace

Même s' il éxiste des cartes SD de 250 Go, un jour vous vous sentirez sûrement à l'étroit. Il éxiste des outils comme mhddfs qui vont vous permettre de fusionner les partitions entre-elles. Je vous renvoie vers ce tuto pour plus d'informations : [Mhddfs - Combine Several Smaller Partition into One Large Virtual Storage](https://www.tecmint.com/combine-partitions-into-one-in-linux-using-mhddfs/)
